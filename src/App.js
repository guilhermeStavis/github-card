import { useState, useEffect } from "react";
import "./App.css";
import InnerCard from "./components/card";
import { Card } from "antd";
import "antd/dist/antd.css";

function App() {
  const [user, setUser] = useState("");
  const [active, setActive] = useState(true);
  const [input, setInput] = useState("");

  let apiUrl = "https://api.github.com/users/";

  useEffect(() => {
    fetch("https://api.github.com/users/frenck")
      .then((res) => res.json())
      .then((res) => {
        setUser(res);
      });
  }, []);

  const handleToggle = () => {
    setActive(!active);
  };

  const search = () => {
    apiUrl += input;
    fetch(apiUrl)
      .then((res) => res.json())
      .then((res) => setUser(res));
  };

  return (
    <div className="App">
      <div>
        <button onClick={handleToggle}>Toggle</button>
        <input
          onChange={(evt) => {
            setInput(evt.target.value);
          }}
          value={input}
          placeholder="Digite um usuário GitHub"
        ></input>
        <button onClick={search}>ok</button>
      </div>
      {active && (
        <Card title="GitHub Card">
          <InnerCard user={user} />
        </Card>
      )}
    </div>
  );
}

export default App;
