import React from "react";
import "./card.css";

const InnerCard = ({ user }) => {
  return (
    <div className="card">
      <img src={user.avatar_url} alt="" />
      <p>{user.name}</p>
      <p>{user.bio}</p>
      <p>{user.location}</p>
    </div>
  );
};

export default InnerCard;
